from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from books import models
from .models import Books


# Create your views here.
class BooksListView(ListView):
    context_object_name = 'books'
    model = models.Books
    template_name = 'books/books_view.html'
