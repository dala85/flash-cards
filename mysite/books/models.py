from django.db import models
from PIL import Image


class Books(models.Model):
    author = models.CharField(max_length=200, null=True, blank=True)
    title = models.CharField(max_length=200, null=True, blank=True)
    section = models.CharField(max_length=200, null=True, blank=True)
    summary = models.TextField(max_length=200, null=True, blank=True)
    image = models.ImageField(default='default.jpg', upload_to='books')

    def __str__(self):
        return self.title
